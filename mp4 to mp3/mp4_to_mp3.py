import os
from moviepy.editor import VideoFileClip

mp4_path = os.path.join(os.getcwd(), 'mp4')
mp3_path = os.path.join(os.getcwd(), 'mp3')

for file_name in os.listdir(mp4_path):
    file_name_mp4 = os.path.join(mp4_path, file_name)
    file_name_mp3 = os.path.join(mp3_path, file_name[:-1] + '3')
    video = VideoFileClip(file_name_mp4)
    video.audio.write_audiofile(file_name_mp3)
