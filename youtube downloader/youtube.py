import bs4 as bs

from PySide2.QtWebEngineWidgets import QWebEnginePage
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QUrl
import pytube # library for downloading youtube videos


class Page(QWebEnginePage):

    def __init__(self, url:str) -> None:
        try:
            self.app = QApplication([])
        except:
            self.app = QApplication.instance()
        QWebEnginePage.__init__(self)
        self.html = ''
        self.loadFinished.connect(self._on_load_finished)
        self.load(QUrl(url))
        self.app.exec_()

    def _on_load_finished(self) -> None:
        self.html = self.toHtml(self.Callable)
        print('Load finished')

    def Callable(self, html_str:str) -> None:
        self.html = html_str
        self.app.quit()


def exact_link(link:str) -> str:
    vid_id = link.split('=')
    # print(vid_id)
    str = ""
    for i in vid_id[0:2]:
        str += i + "="

    str_new = str[0:len(str) - 1]
    index = str_new.find("&")

    new_link = "https://www.youtube.com" + str_new[0:index]
    return new_link


links = [
    "https://www.youtube.com/watch?v=_pi7oqZ6ioU",
    "https://www.youtube.com/watch?v=xHZ7BFBoYCQ",
    "https://www.youtube.com/watch?v=GwfhH8yDZQo",
    "https://www.youtube.com/watch?v=ZOy0YgUDwDg",
    "https://www.youtube.com/watch?v=cZjtRQMEOmI",
    "https://www.youtube.com/watch?v=FhzNSPiqO0M",
    'https://www.youtube.com/watch?v=g-jwWYX7Jlo',
    'https://www.youtube.com/watch?v=dmWfXKJbn9g',
    'https://www.youtube.com/watch?v=K-4A1hi-DuA',
    'https://www.youtube.com/watch?v=26U_seo0a1g',
    'https://www.youtube.com/watch?v=d_S6DKqcU9M',
    'https://www.youtube.com/watch?v=HiyYEVcU1tI',
    'https://www.youtube.com/watch?v=urzSKs-TmBc',
    'https://www.youtube.com/watch?v=754f1w90gQU',
    'https://www.youtube.com/watch?v=ASIT7U463Q0',
    'https://www.youtube.com/watch?v=N2WVHIau77Q',
    'https://www.youtube.com/watch?v=c0ZzN6hxdzo',
    'https://www.youtube.com/watch?v=lsSC2vx7zFQ',
    'https://www.youtube.com/watch?v=3H5BRxsaTm8',
    'https://www.youtube.com/watch?v=Ps4hAQ_Fp5k',
    'https://www.youtube.com/watch?v=RQRfnexHJmk',
    'https://www.youtube.com/watch?v=CMm6tDavSXg',
    'https://www.youtube.com/watch?v=jZn_IrOeHec',
    'https://www.youtube.com/watch?v=bqVxBT-Z4yE'
    'https://www.youtube.com/watch?v=TBuIGBCF9jc',
]

# Scraping and extracting the video links from the given playlists url
playlists = [
    
]
for playlist in playlists:
    page = Page(playlist)
    size = page.contentsSize()

    soup = bs.BeautifulSoup(page.html, 'html.parser')
    links_from_soup = soup.find_all('a', id='thumbnail')
    for count in range(len(links_from_soup)):
        #for link in links_from_soup:
        # not using first link because it is
        # playlist link not particular video link
        if count == 0:
            # count += 1
            continue
        elif count == len(links_from_soup) - 1:
            continue
        else:
            # count += 1
            link = links_from_soup[count]
            vid_src = link['href']
            # print(vid_src)
            # keeping the format of link to be
            # given to pytube otherwise in some cases
            new_link = exact_link(vid_src)
            links.append(new_link)

# print(links)

# downloading each video from the link in the links array
for link in links:
    try:
        yt = pytube.YouTube(link)

        # Downloaded video will be the best quality video
        stream = yt.streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first()
        try:
            stream.download()
            print("Downloaded: ", link)
        except: 
            print('Some error in downloading: ', link)
    except:
        print('some error in yt init ', link)
